// CRUD operations
/*
	CRUD is an acronym for: Create, Read, Update, and Delete
	-Create: is a function that allows users to create a new record in the database
	-Read: is a function that is similar to search function. It allows users to search and retrieve specific records.
	-Update: is a function that is used to modify existing records that are on our database.
	-Delete: is a function that allows users to remove records from a database that is no longer needed.
*/

// Create: INSERT documents
/*
	The mongo shell uses JS for its syntax
	-MongoDB deals with objects as it's structure for documents.
	- We can create documents by providing objects into our methods
	JS Syntax:
		- object.object.method({object})
*/

//Insert one
/*
	Syntax:
	 db.collectionName.insertOne({object});
*/

db.users.insert({
	firstName : "Jane",
	lastName : "Doe",
	age : 21,
	contact : {
		phone : "09127473",
		email : "janedoe@gmail.com"
	}
});

//Insert Many
/*
	Syntax:
		db.collectionName.insertMany([
			{objectA}, {objectB}
		]);
*/

db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age : 76,
			contact : {
				phone : "018308340",
				email: "SHawk@gmail.com"
			},
			course: ["Pythopn", "React", "PHP"],
			department : "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age : 82,
			contact : {
				phone : "0117308989",
				email: "neilarmstrong@gmail.com"
			},
			course: ["Laravel", "React", "SASS"]
		}
	]);

//Read: findd/retrieve documents
/*
	the document will be returned based on their order of storage in the collection
*/

//Find all documents
/*
	Syntax:
		db.collectionName.find()
*/

db.users.find();

// Find using single parameter
/*
	Syntax:
		db.collectionName.find({
			field : value
		})
*/

db.users.find({
	name : "Stephen"
});

// Find using multiple parameters
/*
	Syntax:
		db.collectionName.find({
			fieldA : valueA,
			fieldB : valueB
		});
*/
db.users.find({
	lastName : "Armstrong",
	age : 82
});

//Find + pretty method
/*
	the "pretty" method allows us to be able to view the documents returned by our terminal in a "prettier" format.

	Syntax:
		db.collectionName.find({ field:value }).pretty();
*/

db.users.find({
	lastName : "Armstrong",
	age : 82
}).pretty();

//Update: edit a document

//Update one: update a single document
/*
	Syntax:
		db.collectionName.update(
		{criteria}, 
		{$set: 
			{field : value}
		}
	);
*/

db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age : 0,
	contact : {
		phone : "000000",
		email : "test@gmail.com"
	},
	courses : [],
	department : "none"
});


db.users.updateOne(
	{firstName : "Test"},
	{
		$set:{
			firstName : "Bill",
			lastName : "Gates",
			age : 65,
			contact:{
				phone : "87654321",
				email :"bill@gmail.com"
			},
			courses : ["PHP", "Laravel", "HTML"],
			department : "Operations",
			status : "Active"
		}
	}
);

//Update many
/*
	Syntax:
		db.collectionName.updateMany({criteria}, {$set{field : value}})
*/

db.users.updateMany(
	{department : "none"},
	{
		$set{
			department: "HR"
		}
	}
);

db.users.find().pretty();

//Replace one
/*
	Replaces the whole document
	-if updateOne updates specific fields, replaceOne replaces the whole document
	-if u
*/

db.users.replaceOne(
	{firstName: "Bill"},
	{
		firstName: "Bill",
		lastName: "Gates",
		age : 65,
		contact : {
			phone: "12345678",
			email : "billyG@microsoft.com"
		},
		courses : ["PH", "Laravel", "HTML"],
		department : "Operations"
	}
);

//Delete
/*
	it is good to practice sof deletion or archiving our documents instead of deleting or removing them from the system
*/
db.users.insert({
	firstName : "Test"
});

/*
	Syntax:
	db.collectionName.deleteOne({criteria});
*/

db.users.deleteOne({ firstName : "Test"});

//Delete many
/*
	Syntax:
	db.collectionName.deleteMany({criteria});
*/

db.user.deleteMany({
	firstName : "Bill"
});

//Delete all
/*
	Deletes all documents
	Syntax:
		db.collectionName.deleteMany({})
*/

//Advances Queries
/*
	Retrieving data with complex data structure is also a good skill for any developer to have
	-Real world examples can be as complex as having two or more layers of nested objects
	-learning to query these kinds of data is also essential to ensure we are able to retrieve any information that we would need in our application
*/
//Query an embeded document
// an embeded document are those types of documents that contain a document inside a document

db.users.find({
	contact : {
		phone : "018308340",
		email: "SHawk@gmail.com"
	}
});

//Query on a nested field
db.users.find({
	"contact.email" : "SHawk@gmail.com"
});

//Querying an array with exact elements
db.users.find({courses : ["CSS", "Javascript", "Python"]});

//Querying without regard of order
db.users.find({cousrses: {$all: ["React", "Python"]}});

//Querying an Embeded Array
db.users.insert({
	namearr : [{
		nameA : "Juan"
	},{
		nameB : "Tamad"
	}]
});

db.users.find({
	nameArr: {
		nameA : "Juan"
	}
});
