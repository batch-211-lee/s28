db.rooms.insertOne({
    name : "single",
    accomodates : 2,
    price : 1000,
    description : "A simple room with all the basic neccessities",
    rooms_available : 10,
    isAvailable : false
});

db.rooms.insertMany([
    {name : "double",
        accomodates : 3,
        price : 2000,
        description : "A room fit for a small damily going on a vacation",
        rooms_available : 5,
         "isAvailable" : false},
    {name : "queen",
        accomodates : 4,
        price : 4000,
        description : "A room with a queen sized bed, perfect for a simple getaway",
        rooms_available : 15,
        isAvailable : false}
]);

db.getCollection(rooms).find({});

db.rooms.find({
    name : "double"
});

db.rooms.updateOne(
    {
        _id : ObjectId("6345fd2945c762f2bd3ae676")
    },
    {
        $set:{
            rooms_available : 0
        }
    }
 );

db.rooms.deleteMany({
    rooms_available : 0
});